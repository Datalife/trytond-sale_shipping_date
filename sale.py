# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    planned_shipping_date = fields.Date('Planned Shipping Date',
        states={
            'readonly': Eval('state') != 'draft'
        },
        depends=['state'])

    def _group_shipment_key(self, moves, move):
        SaleLine = Pool().get('sale.line')
        line_id, _ = move
        line = SaleLine(line_id)

        res = super()._group_shipment_key(moves, move)

        return res + (('planned_shipping_date',
            line.manual_shipping_date_used),
        )

    def _get_shipment_sale(self, Shipment, key):
        keys = list(key)
        for k in keys:
            if 'planned_shipping_date' in k:
                keys.remove(k)
        key = tuple(keys)
        return super()._get_shipment_sale(Shipment, key=key)

    @classmethod
    def set_sale_date(cls, sales):
        for sale in sales:
            if not sale.sale_date and sale.planned_shipping_date:
                cls.write([sale], {
                        'sale_date': sale.planned_shipping_date,
                        })
        return super().set_sale_date(sales)


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    manual_shipping_date = fields.Date('Planned Shipping Date',
        states={
            'invisible': Eval('type') != 'line',
            'readonly': Eval('sale_state') != 'draft'
        },
        depends=['type', 'sale_state'])

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        if table_h.column_exist('planned_shipping_date'):
            table_h.column_rename('planned_shipping_date',
             'manual_shipping_date')

        super().__register__(module_name)

    @fields.depends('sale', '_parent_sale.planned_shipping_date')
    def on_change_with_manual_shipping_date(self):
        if self.sale:
            return self.sale.planned_shipping_date

    def get_move(self, shipment_type):
        move = super().get_move(shipment_type)
        if move and not self.moves:
            move.planned_date = self.manual_shipping_date_used
        return move

    @property
    def manual_shipping_date_used(self):
        return self.manual_shipping_date or \
            self.sale.planned_shipping_date or \
            self.shipping_date
