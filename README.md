datalife_sale_shipping_date
===========================

The sale_shipping_date module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_shipping_date/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_shipping_date)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
